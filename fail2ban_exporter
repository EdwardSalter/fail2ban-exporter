#!/usr/bin/node

const http = require("http");
const { spawn } = require("child_process");

async function getFail2banData() {
  const jails = await getFail2banJails();
  if (jails.length === 0) throw new Error("No jails found");

  const statuses = await Promise.all(jails.map(getStatusForJail));

  return formatStatusesForPrometheus(statuses);
}

function formatStatusesForPrometheus(statuses) {
  const currentFailedLines = formatLines(
    statuses,
    "currently_failed",
    "currentFailed"
  );
  const totalFailedLines = formatLines(statuses, "total_failed", "totalFailed");
  const currentBannedLines = formatLines(
    statuses,
    "currently_banned",
    "currentBanned"
  );
  const totalBannedLines = formatLines(statuses, "total_banned", "totalBanned");

  return `# HELP fail2ban_currently_failed Number of log lines in current time window that represent a failure
# TYPE fail2ban_currently_failed gauge
${currentFailedLines}
# HELP fail2ban_total_failed Total number of log lines since fail2ban started that represent a failure
# TYPE fail2ban_total_failed gauge
${totalFailedLines}
# HELP fail2ban_currently_banned Number of IP addresses that have been banned in the current operating time window
# TYPE fail2ban_currently_banned gauge
${currentBannedLines}
# HELP fail2ban_total_banned Number of IP addresses that have been banned since fail2ban started
# TYPE fail2ban_total_banned gauge
${totalBannedLines}`;
}

function formatLines(statuses, metric, field) {
  return statuses
    .map(
      (status) =>
        `fail2ban_${metric}{jail="${status.jail}"} ${status[field].toFixed(1)}`
    )
    .join("\n");
}

const requestListener = async function (req, res) {
  try {
    const output = await getFail2banData();

    res.writeHead(200);
    res.end(output);
  }
  catch (e) {
    console.error(e);
    res.writeHead(500);
    res.end(e.toString());
  }
};

const port = 9180;
const host = "0.0.0.0";

const server = http.createServer(requestListener);
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});

async function getFail2banJails() {
  const statusString = await executeCommand("fail2ban-client", ["status"]);
  return getJailListFromOutput(statusString);
}

// `- Jail list:   dovecot, mysqld-auth, nginx-botsearch, nginx-http-auth, postfix, recidive, sendmail-auth, sshd
function getJailListFromOutput(output) {
  const listLine = /Jail list:\s*(.*)/;
  const match = listLine.exec(output);
  if (!match || !match[1])
    throw new Error(
      "Unable to find list of jails in fail2an-client status output"
    );

  return match[1].split(",").map((s) => s.trim());
}

async function getStatusForJail(jail) {
  /*
    |- Filter
    |  |- Currently failed: 0
    |  |- Total failed:     0
    |  `- File list:        /var/log/mail.log
    `- Actions
       |- Currently banned: 0
       |- Total banned:     0
       `- Banned IP list:
     */
  const output = await executeCommand("fail2ban-client", ["status", jail]);

  const currentFailed = getNumberFromJailStatus("Currently failed", output);
  const totalFailed = getNumberFromJailStatus("Total failed", output);
  const currentBanned = getNumberFromJailStatus("Currently banned", output);
  const totalBanned = getNumberFromJailStatus("Currently banned", output);
  return {
    currentFailed,
    totalFailed,
    currentBanned,
    totalBanned,
    jail,
  };
}

function getNumberFromJailStatus(lineText, output) {
  const regex = new RegExp(`${lineText}:\\s*(\\d+)`, "m");
  const match = regex.exec(output);
  if (!match || !match[1])
    throw new Error(
      `Failed to find number in output string starting with \`${lineText}\``
    );
  return Number(match[1]);
}

function executeCommand(command, args) {
  return new Promise((resolve, reject) => {
    const status = spawn(command, args);
    let output = "";

    status.stdout.on("data", (data) => {
      output += data;
    });

    status.stderr.on("data", (data) => {
      console.error(`stderr: ${data}`);
    });

    status.on("close", (code) => {
      if (code !== 0) {
        reject(
          new Error(
            `Non-zero exit code from executing \`${command} ${args.join(" ")}\``
          )
        );
      }

      resolve(output);
    });
  });
}
